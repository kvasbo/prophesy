//
//  AddPhotoController.swift
//  prophesy
//
//  Created by Audun Kvasbø on 11.03.2018.
//  Copyright © 2018 Rettvendt AS. All rights reserved.
//

import UIKit

class AddPhotoController: UITableViewController {
    
    var photos = [String]()
    var selectedPhoto: String?

    override func viewDidLoad() {
        self.initPhotos();
        tableView.delegate = self
        tableView.dataSource = self
        super.viewDidLoad()
    }
    
    func initPhotos() {
        photos.append("airplane")
        photos.append("fireworks")
        photos.append("newborn")
        photos.append("beach")
        photos.append("candles")
        photos.append("party_glass")
        photos.append("party_table")
        photos.append("wedding_couple")
        photos.append("wedding_table")
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "photoChooseCell", for: indexPath) as! PhotoChooseCell
        let index = indexPath.item
        cell.imageTitle = photos[index]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PhotoChooseCell
        selectedPhoto = cell.imageTitle
        performSegue(withIdentifier: "unwindToEditor", sender: self)
    }

}
