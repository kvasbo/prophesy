//
//  EventListCell.swift
//  prophesy
//
//  Created by Audun Kvasbø on 09.03.2018.
//  Copyright © 2018 Rettvendt AS. All rights reserved.
//

import UIKit
import SwiftMoment
// import QuartzCore

class EventListCell: UITableViewCell {
   
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelDaysToGo: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    
    var event: Event? {
        didSet {
            labelDescription.text = event?.title
            labelDaysToGo.text = calculateDaysLeft()
            self.setPhoto()
        }
    }
    
    func setPhoto() {
        if(event?.image != nil) {
            if let image = UIImage(data: (event?.image!)!) {
                
                var shadowColor = UIColor.white
                eventImage.image = image
                
                if(image.isDark) {
                    labelDescription.textColor = UIColor.white
                    labelDaysToGo.textColor = UIColor.white
                    shadowColor = UIColor.black
                }
                
                /*
                let context = CIContext(options: nil)
                if let currentFilter = CIFilter(name: "CIPhotoEffectProcess") {
                    let beginImage = CIImage(image: image)
                    currentFilter.setValue(beginImage, forKey: kCIInputImageKey)                    
                    if let output = currentFilter.outputImage {
                        if let cgimg = context.createCGImage(output, from: output.extent) {
                            let processedImage = UIImage(cgImage: cgimg)
                            eventImage.image = processedImage
                            if(processedImage.isDark) {
                                labelDescription.textColor = UIColor.white
                                labelDaysToGo.textColor = UIColor.white
                                shadowColor = UIColor.black
                            }
                        }
                    }
                }
                */
                
                labelDaysToGo.layer.masksToBounds = false
                labelDaysToGo.layer.shadowColor = shadowColor.cgColor
                labelDaysToGo.layer.shadowRadius = 3
                labelDaysToGo.layer.shadowOpacity = 1.0
                labelDaysToGo.layer.shadowOffset = CGSize.zero
                labelDaysToGo.layer.shouldRasterize = true
                labelDaysToGo.isOpaque = false
                labelDescription.layer.shadowColor = shadowColor.cgColor
                labelDescription.layer.shadowRadius = 3
                labelDescription.layer.shadowOpacity = 1.0
                labelDescription.layer.shadowOffset = CGSize.zero
                labelDescription.layer.shouldRasterize = true
            }
        }
    }
    
    override func awakeFromNib() {
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
        super.awakeFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    @objc func update() {
        if (self.event != nil) {
            self.labelDaysToGo.text = calculateDaysLeft()
        }
    }
    
    func setEvent(event: Event) {
        self.event = event
    }
    
    private func calculateDaysLeft() -> String {
        let now = moment()
        if (self.event != nil && self.event?.date != nil) {
            let startOfNow = now.startOf(TimeUnit.Days)
            let then = moment((self.event?.date)!)
            let startOfThen = then.startOf(TimeUnit.Days)
            let diff = startOfThen.intervalSince(startOfNow)
            let roundDiff = Int(round(diff.days)) // Round, as this will nicely handle summer time (other times, it will be exact
            return String(roundDiff)
        } else {
            return "nil"
        }
    }
}
