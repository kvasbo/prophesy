//
//  PhotoChooseCell.swift
//  prophesy
//
//  Created by Audun Kvasbø on 11.03.2018.
//  Copyright © 2018 Rettvendt AS. All rights reserved.
//

import UIKit

class PhotoChooseCell: UITableViewCell {
    

    @IBOutlet weak var photoChooseImage: UIImageView!
    
    var imageTitle: String? {
        didSet {
            self.updateImage()
        }
    }
    
    func updateImage() {
        let image = UIImage(named: self.imageTitle!, in: Bundle(identifier: "Photos"), compatibleWith: nil)
        self.photoChooseImage.image = image
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
}
