//
//  NewEventController.swift
//  prophesy
//
//  Created by Audun Kvasbø on 06.03.2018.
//  Copyright © 2018 Rettvendt AS. All rights reserved.
//

import UIKit
import CoreData

class NewEventController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate {

    //MARK: Variables
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var eventDatePicker: UIDatePicker!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var imageScrollView: UIScrollView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let imagePicker = UIImagePickerController()
    
    var editMode: Bool = false {
        didSet {
            if (editMode == true) {
                 self.title = "Edit"
            }
        }
    }
    
    var event: Event?
    
    var photoTitle: String? {
        didSet {
            self.setPhotoFromTitle()
        }
    }
    
    var scrollViewWidth = CGFloat(0) // Hold actual size of image view
    
    //MARK: View controller
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        titleField.delegate = self
        
        // Setup image box
        imageScrollView.minimumZoomScale = 1.0
        imageScrollView.maximumZoomScale = 1.0
        imagePicker.delegate = self
        imageScrollView.delegate = self
        
        if (titleField?.text?.isEmpty)!{
            saveButton.isEnabled = false
        }
        // Do any additional setup after loading the view.
        if let actualEvent = event {
            titleField.text = actualEvent.title
            eventDatePicker.date = actualEvent.date!
            if (event?.image != nil) {
                eventImageView.image = UIImage(data: (event?.image)!)
            }
            saveButton.isEnabled = true
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollViewWidth = imageScrollView.frame.size.width
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Set photo from string
    func setPhotoFromTitle() {
        if let image = UIImage(named: photoTitle!, in: Bundle(identifier: "Photos"), compatibleWith: nil) {
            // Zoom!
            imageScrollView.minimumZoomScale = 1
            imageScrollView.maximumZoomScale = 1
            imageScrollView.setZoomScale(1, animated: false)
            eventImageView.image = image
        } else {
            photoTitle = nil
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if !text.isEmpty{
            saveButton.isEnabled = true
        } else {
            saveButton.isEnabled = false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: UIScrollViewDelegate
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return eventImageView
    }

    //MARK: UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Expecded blablabla \(info)")
        }
        
        // Get scale
        let width = selectedImage.size.width
        let scale = imageScrollView.frame.size.width / width
        
        // Zoom!
        imageScrollView.minimumZoomScale = scale
        imageScrollView.maximumZoomScale = 1
        imageScrollView.setZoomScale(scale, animated: false)
        
        // Set image
        eventImageView.image = selectedImage
        
        dismiss(animated: true, completion: nil)
    }
    
    // Get cropped data from the image field, for storing
    private func getCroppedImageData() -> UIImage? {
        if (eventImageView.image == nil) {
            return nil
        }
        let scale:CGFloat = 1 / imageScrollView.zoomScale
        let x:CGFloat = imageScrollView.contentOffset.x * scale
        let y:CGFloat = imageScrollView.contentOffset.y * scale
        let width:CGFloat = imageScrollView.frame.size.width * scale
        let height:CGFloat = imageScrollView.frame.size.height * scale
        let rect = CGRect(x: x, y: y, width: width, height: height)
        print("rect", rect.debugDescription, "scale", scale.description)
        // let croppedCGImage = eventImageView.image?.cgImage?.cropping(to: CGRect(x: x, y: y, width: width, height: height))
        // let croppedImage = UIImage(cgImage: croppedCGImage!)
        let croppedImage = eventImageView.image?.croppedImage(inRect: rect)
        return croppedImage
    }
    
    func getSnapShot(view:UIView) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, true, 0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    //MARK: Action
    @IBAction func buttonUserPhotosPressed(_ sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    //Save event to CoreData
    @IBAction func saveEvent(_ sender: UIBarButtonItem) {
       
        let context = appDelegate.persistentContainer.viewContext
        var eventToSave: NSManagedObject?
        
        if(!self.editMode) {
            let entity = NSEntityDescription.entity(forEntityName: "Event", in: context)
            eventToSave = NSManagedObject(entity: entity!, insertInto: context)
        } else {
            eventToSave = self.event
        }
        
        eventToSave?.setValue(titleField.text, forKey: "title")
        eventToSave?.setValue(eventDatePicker.date, forKey: "date")
        
        if(eventImageView.image != nil) {
            if let data  = UIImagePNGRepresentation(getSnapShot(view: imageScrollView)) {
                eventToSave?.setValue(data, forKey: "image")
            }
            /*
            if let data = UIImagePNGRepresentation(self.getCroppedImageData()!) {
                eventToSave?.setValue(data, forKey: "image")
            }
             */
        }
        
        do {
            try context.save()
            performSegue(withIdentifier: "unwindToMain", sender: self)
        } catch {
            print("Failed saving")
        }
    }
    
    @IBAction func unwindToEditor(sender: UIStoryboardSegue)
    {
        if let sourceViewController = sender.source as? AddPhotoController {
            if (sourceViewController.selectedPhoto != nil) {
                self.photoTitle = sourceViewController.selectedPhoto
            }
        }
    }
    
}
