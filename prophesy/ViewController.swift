//
//  ViewController.swift
//  prophesy
//
//  Created by Audun Kvasbø on 06.03.2018.
//  Copyright © 2018 Rettvendt AS. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context: NSManagedObjectContext?
    var events = [NSManagedObject]()
    
    var eventToEdit: Event?
    
    fileprivate lazy var fetchedResultsController: NSFetchedResultsController<Event> = {
        // Create Fetch Request
        let fetchRequest: NSFetchRequest<Event> = Event.fetchRequest()
        
        // Configure Fetch Request
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        
        // Create Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: appDelegate.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        
        // Configure Fetched Results Controller
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()

    override func viewDidLoad() {
        context = appDelegate.persistentContainer.viewContext
        super.viewDidLoad()
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            print("Fucka fetch")
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    private func fetchRecordsForEntity(_ entity: String) -> [NSManagedObject] {
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        
        // Helpers
        var result = [NSManagedObject]()
        
        do {
            // Execute Fetch Request
            let records = try self.context?.fetch(fetchRequest)
            
            if let records = records as? [NSManagedObject] {
                result = records
            }
            
        } catch {
            print("Unable to fetch managed objects for entity \(entity).")
        }
        
        return result
    }
    

    // Count
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfItems = self.fetchedResultsController.fetchedObjects?.count
        return numberOfItems!
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            self.showEditor(row: index)
        }
        edit.backgroundColor = UIColor.gray
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { action, index in
            self.doDelete(row: index)
        }
        delete.backgroundColor = UIColor.red
        
        return [delete, edit]
    }
    
    private func showEditor(row: IndexPath) {
        let currentCell = tableView.cellForRow(at: row) as! EventListCell
        eventToEdit = currentCell.event
        performSegue(withIdentifier: "showEventEditor", sender: self)
    }
    
    private func doDelete(row: IndexPath) {
        let currentCell = tableView.cellForRow(at: row) as! EventListCell
        do {
            let eventToDie = currentCell.event as NSManagedObject?
            context?.delete(eventToDie!)
            try context?.save()
        } catch {
            print("kunne ikke slette")
        }
    }
    
    /**
     Get the cells
    */
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventListCell
        let event = fetchedResultsController.object(at: indexPath)
        cell.setEvent(event: event)
        return cell
    }
    
    @IBAction func unwindToMainMenu(sender: UIStoryboardSegue)
    {
        // let sourceViewController = sender.source
        // Pull any data from the view controller which initiated the unwind segue.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showEventEditor" && eventToEdit != nil) {
            let destinationVC = segue.destination as! NewEventController
            destinationVC.event = eventToEdit
            destinationVC.editMode = true
            eventToEdit = nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

